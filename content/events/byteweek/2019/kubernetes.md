---
startdate:  2019-02-01
starttime: "18:00"
enddate:  2019-02-01
linktitle: "Pre-FOSDEM warmup with Kubernetes"
title: "Pre-FOSDEM warmup with Kubernetes"
location: "HSBXL"
eventtype: "Talk"
price: "Free"
series: "byteweek2019"
image: "kubernetes.png"
--- 

Let’s kick off Fosdem with some interesting Kubernetes talks!

# Agenda:

## 1: Running MySQL on Kubernetes: comparison of the Oracle, Presslabs, and Percona solutions
-- Mykola Marzhan, Tech Lead at Percona, one of the authors of the Percona Operator

Running databases in Kubernetes attracts a lot of attention today. Orсhestration of MySQL on Kubernetes is no way a straightforward process. There are several good MySQL based solutions in the open source world, made by Oracle, Presslabs, and Percona. Having common base, they differ in self-healing capabilities, multimaster and backup/restore support, etc. So let’s make a fair comparison to figure out the pros and cons of their current state.

## 2: Troubleshooting Kubernetes apps
-- Michael Hausenblas, Developer Advocate for Kubernetes and a CNCF ambassador

Kubernetes makes it easy to run cloud-native apps in a portable and resilient way. But what if something fails? How to find out what caused a CrashLoopBackOff error? What can be done about a service that is not reachable? How to debug a stateful application? How to do fault injection in a microservices setup? In this talk, we’ll discuss the most common reasons an app in Kubernetes fails and how to address them.

## 3: Apache Spark on Kubernetes -- Avoiding the pain of YARN
-- Holden is a transgender Canadian open source developer advocate @ Google with a focus on Apache Spark, Airflow, and related "big data" tools.)

Apache Spark is one of the most popular big data tools, and
starting last year has had integrated support for running on
Kubernetes. This talk will introduce some of the use cases of
Apache Spark quickly (machine learning, ETL, etc.) and then look at
the current cluster managers Spark runs on and their limitations.
Most of the focus will be around running non-Java code, and the
challenges associated with dependencies along with general
challenges like scale-up & down. It’s not all sunshine and
roses though, I will talk about some of the limitations of our current approach and the work being done to improve
this.


    Last talk will be announced soon!

# Drinks and food
Drinks and food are offered by **Redhat**.

# Registration
https://www.meetup.com/kubernetes-belgium/events/257513282
