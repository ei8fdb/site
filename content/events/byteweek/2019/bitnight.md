---
startdate:  2019-02-01
starttime: "21:00"
enddate:  2019-02-01
linktitle: "Bitnight"
title: "Bitnight"
location: "HSBXL"
eventtype: "Thé Dansant"
price: "Free"
series: "byteweek2019"
aliases: [/bitnight]
--- 

# Info
Pre-[Bytenight](/bytenight)...but smaller...

It will mix attendees from HSBXL's event that day, including:
 - Lua on a Stick hackathon
 - ZeroMQ hackathon
 - GNU Radio Hackfest
 - Blockstack workshop
 - Kubernetes meetup
 - Visitors to FOSDEM

# Music
‘Jonny and the Bomb’: “Sound Artist”, The Guardian

# Info
See Byteweek for details on HSBXL's pre-FOSDEM events:
https://hsbxl.be/events/byteweek/2019/

# SAT BYTENIGHT
Dont forget Bytenight on SAT 2nd February. Now in its 10th incarnation, it is the largest social event taking place during FOSDEM:
https://hsbxl.be/events/byteweek/2019/bytenight/
Music by 'Jonny and the Bomb': "Sound Artist", The Guardian

# Supporting
Please support this event (and others) through promotion via our official social media pages:
 - [Meetup](https://www.meetup.com/hackerspace-Brussels-hsbxl/events/)
 - [Meetup](https://www.meetup.com/hackerspace-Brussels-hsbxl/events/258068023/)