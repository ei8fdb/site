---
linktitle: "Tech with Kids"
title: "Tech with kids"
---

A get together with kids, teens and parents to work on tech projects and learn new stuff.

## Upcoming events
{{< events when="upcoming" series="Tech With Kids" >}}

## Past events
{{< events when="past" series="Tech With Kids" >}}
