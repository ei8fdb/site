---
eventid: techtue515
startdate:  2019-04-02
starttime: "19:00"
linktitle: "TechTue 516"
title: "TechTuesday 516"
price: ""
image: "techtuesday.png"
series: "TechTuesday"
eventtype: "Social get-together"
location: "HSBXL"
---

Techtuesdays are a social meet-up sort of thing, just walk in for a talk, club-mate, latest news, or to show off your latest pet-project ... or you can always stay home and read slashdot.
