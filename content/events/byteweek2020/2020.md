---
startdate:  2020-01-24
starttime: "19:00"
enddate: 2020-02-01
#endtime: "05h"
allday: true
linktitle: "ByteWeek 2020"
title: "Byteweek 2020"
location: "HSBXL"
eventtype: "Eight bits of awesomeness"
price: ""
image: "byteweek.png"
series: "byteweek2020"
start: "true"
#aliases: [/byteweek] 
---

The week before Fosdem conference, HSBXL compiles ByteWeek.  
Eight 'Bitdays' of hackatons, workshops and talks.  
We end our week with the notorious [Bytenight](/bytenight)!



# Call for participation
If you want to have a talk, workshop or hackaton,
send a mail to **contact@hsbxl.be** with your proposal.


# calendar

## Monday January 27th
{{< events series="byteweek2020" when="2020-01-27" showtime="true" showallday="true" >}}
## Tuesday January 28th
{{< events series="byteweek2020" when="2020-01-28" showtime="true" showallday="true" >}}
## Wednesday January 29th
{{< events series="byteweek2020" when="2020-01-29" showtime="true" showallday="true" >}}
## Thursday January 30th
{{< events series="byteweek2020" when="2020-01-30" showtime="true" showallday="true" >}}
## Friday January 31st
{{< events series="byteweek2020" when="2020-01-31" showtime="true" showallday="true" >}}
## Saturday February 1st
{{< events series="byteweek2020" when="2020-02-01" showtime="true" showallday="true" >}}



