---
eventid: cybsec1
startdate:  2019-06-05
starttime: "19:00"
linktitle: "Cyber Security Co-Learning"
title: "Cyber Security Co-Learning"
price: ""
image: "cybersecuritycolearning.jpg"
series: "cybsec"
eventtype: "Co-Learning"
location: "HSBXL"
---


Hello!

Are you working or interested in Cyber Security? And are you looking for ways to learn more about it?  
During our co-learning we do self-study, work on projects alone or with others, and present our work to the others.  

Sound cool? then this meetup is for you :-)  

We are hosted by Hackerspace <3, there is a fridge on location with drinks - make sure to bring some coins :-)  
The rules: Bring your own laptop and respect the code of conduct → http://www.opentechschool.org/code-of-conduct.  
If you don't have a project to work on, or if you're otherwise unsure how to get started check out to learning materials collected or/and let's figure it out together during the meetup. The hardest part is starting :-)

Learning materials are collected in a google sheet, feel to add anything:  
https://docs.google.com/document/d/1v52FqM8fZWpOrWHs6cxeeWAUGtc62YL25MHva9h0W2w/edit?usp=sharing

We are also looking for support, feel free to reach out:

- if you are an experienced security engineer/pentester/expert and would like to organize a workshop
- If you have learning material/methods you would like to share

Meetup page: https://www.meetup.com/Cyber-Security-Brussels/events/tzqpzqyzjbhb