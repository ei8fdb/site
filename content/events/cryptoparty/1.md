---
startdate:  2019-03-18
starttime: "18:30"
linktitle: "Cryptoparty 1"
title: "Cryptoparty 1"
price: ""
image: "cryptoparty.png"
series: "Cryptoparty"
eventtype: "Party like it's December 31st, 1983"
location: "HSBXL"
---

# What?
CryptoParty is a decentralized movement with events happening all over the world. The goal is to pass on knowledge about protecting yourself in the digital space. This can include encrypted communication, preventing being tracked while browsing the web, and general security advice regarding computers and smartphones.  
To try the programs and apps at the CryptoParty/PrivacyCafe bring your laptop or smartphone.

# Why?
Privacy is the room in which ideas develop, where you can reflect on those ideas whenever you choose. This room is not only physical, but digital as well. Neither governments nor corporations respect that. But we do.


-- https://www.cryptoparty.in
