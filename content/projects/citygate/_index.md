---
title: "Move to Citygate"
linktitle: "Move to Citygate"
state: running
image: "citygate.png"
---

HSBXL is moving to Citygate!

# Progress

- [X] Choose [moving days](/events/moving)
- [X] Remove [floor](IMG_20181120_183839.jpg)
- [X] Remove [wall between first and second room](IMG_20181204_180039.jpg)
- [X] Close the [window next to the door](IMG_0021_result.JPG)
- [X] Fill the [airgaps above the windows](IMG_0023_result.JPG)
- [X] Install Wireless België (temporary internet for during the move)
- [X] Make truck reservation
- [X] Uninstall heaters
- [X] Uninstall TV's & other stuff hanging on the walls
- [X] Make all desks ready to move
- [X] Uninstall electronics lab
- [X] Uninstall Wireless België @ Molenbeek
- [X] Pickup moving truck
- [X] Actual move
- [X] Return moving truck
- [X] Hang the 'strokengordijn'
- [X] Uninstall elevator box @ Molenbeek
- [X] Uninstall water pipe at @ Molenbeek
- [X] Arrange access / copy keys
- [X] Install [Wireless België](uplink.jpg)
- [X] Install internet / wifi
- [X] Setup the kitchen / the bar
- [X] Hang the screens
- [X] Fix the waterleak
- [X] Install rockwool on our roof
- [X] Setup Hardcorener
- [ ] Install trollingpage
- [ ] Create cool street signs to put between south trainstation and the space, mention meters left...
- [ ] Install 'elevator box' @ Citygate
- [ ] Install '[black knight](/projects/black_knight)' @ Citygate
- [ ] Hang whiteboards
- [ ] Replace our door with metal door

# Citygate in the media

- https://www.bruzz.be/en/videoreeks/bruzz-24-2017-06-07/video-citygate-must-breathe-new-life-desolate-industrial-zone
- https://bx1.be/news/anderlecht-le-studio-citygate-occupe-temporairement-par-des-artistes-sportifs-et-artisans
- http://canal.brussels/fr/content/studio-citygate-une-des-plus-grandes-occupations-temporaires-en-belgique
- http://screen.brussels/en/film-commission/movieset/citygate

# Moving related events
{{< events series="moving" >}}

# Photos
{{< gallery "images" >}}

