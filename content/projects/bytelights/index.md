---
title: "ByteLights"
linktitle: "ByteLights"
state: running
maintainer: "Jegeva"
---

This project describes the boomboom lights as used during bytenight 2019


# Hardware

esp8266 ftw. i used wemoses D1.

## Dedicated WiFi

Just because we have weird people on our network during bytenight.

## RGB strips
install fastled, install esp8266 support in your arduino IDE, be lazy :tada:
nothing fancy, just a modified fastled demoreel

~~~~
#include "FastLED.h"
#include <ESP8266WiFi.h>
#include <WiFiUdp.h>

const int ledPin =  LED_BUILTIN;
char ssid[] = ""
char pass[] = ""; //Password of your Wi-Fi router
byte packetBuffer[16];
const long interval = 1000;
volatile int ledState = LOW;
WiFiUDP Udp;
volatile unsigned long previousMillis = 0;        // will store last time LED was updated
volatile unsigned long currentMillis;
char incomingPacket[255];

void connectAP(){
  Serial.println();
  Serial.println();
  Serial.print("Connecting to...");
  Serial.println(ssid);
  WiFi.begin(ssid, pass);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("Wi-Fi connected successfully");
  Serial.println("IP address: ");
    Serial.println(WiFi.localIP()); 
    Serial.println(WiFi.subnetMask());
  }



FASTLED_USING_NAMESPACE

// FastLED "100-lines-of-code" demo reel, showing just a few 
// of the kinds of animation patterns you can quickly and easily 
// compose using FastLED.  
//
// This example also shows one easy way to define multiple 
// animations patterns and have them automatically rotate.
//
// -Mark Kriegsman, December 2014

#if defined(FASTLED_VERSION) && (FASTLED_VERSION < 3001000)
#warning "Requires FastLED 3.1 or later; check github for latest code."
#endif

#define DATA_PIN    3
//#define CLK_PIN   4
#define LED_TYPE    WS2811
#define COLOR_ORDER GRB
#define NUM_LEDS    300
CRGB leds[NUM_LEDS];

#define BRIGHTNESS          96
#define FRAMES_PER_SECOND  120

void setup() {
  delay(500); // 3 second delay for recovery500
  // tell FastLED about the LED strip configuration
  FastLED.addLeds<LED_TYPE,DATA_PIN,COLOR_ORDER>(leds, NUM_LEDS).setCorrection(TypicalLEDStrip);
  //FastLED.addLeds<LED_TYPE,DATA_PIN,CLK_PIN,COLOR_ORDER>(leds, NUM_LEDS).setCorrection(TypicalLEDStrip);

  // set master brightness control
  FastLED.setBrightness(BRIGHTNESS);

  //Serial.begin(115200); 

  pinMode(LED_BUILTIN, OUTPUT); 
  connectAP();
  digitalWrite(ledPin, ledState);
  Udp.begin(1234);  
}


// List of patterns to cycle through.  Each is defined as a separate function below.
typedef void (*SimplePatternList[])();
SimplePatternList gPatterns = { rainbow, rainbowWithGlitter, confetti, sinelon, juggle, bpm };

uint8_t gCurrentPatternNumber = 0; // Index number of which pattern is current
uint8_t gHue = 0; // rotating "base color" used by many of the patterns
  
void loop()
{
  gPatterns[gCurrentPatternNumber]();
  currentMillis = millis();
  int packetSize = Udp.parsePacket();
 if (packetSize)
  {

    int len = Udp.read(incomingPacket, 255);
    if (len > 0)
    {
     incomingPacket[len] = 0;
    if(incomingPacket[0] =='b'){

      for(int i = 0;i<NUM_LEDS;i++){
        leds[ i ] += CRGB::White;
        }
      addGlitter(255);
     if (ledState == LOW) {
        ledState = HIGH;
      } else {
        ledState = LOW;
      }
     digitalWrite(ledPin, ledState);
     }
    }
  }

  if (currentMillis - previousMillis >= interval) {
    // save the last time you blinked the LED
    previousMillis = currentMillis;
    int s = WiFi.status();
    if( (s == WL_DISCONNECTED) || (s == WL_CONNECTION_LOST) ){
     // WiFi.stop();  
      connectAP();
    }
  }

  
  // Call the current pattern function once, updating the 'leds' array
 

  // send the 'leds' array out to the actual LED strip
  FastLED.show();  
  // insert a delay to keep the framerate modest
  FastLED.delay(1000/FRAMES_PER_SECOND); 

  // do some periodic updates
  EVERY_N_MILLISECONDS( 20 ) { gHue++; } // slowly cycle the "base color" through the rainbow
  EVERY_N_SECONDS( 10 ) { nextPattern(); } // change patterns periodically
}

#define ARRAY_SIZE(A) (sizeof(A) / sizeof((A)[0]))

void nextPattern()
{
  // add one to the current pattern number, and wrap around at the end
  gCurrentPatternNumber = (gCurrentPatternNumber + 1) % ARRAY_SIZE( gPatterns);
}

void rainbow() 
{
  // FastLED's built-in rainbow generator
  fill_rainbow( leds, NUM_LEDS, gHue, 7);
}

void rainbowWithGlitter() 
{
  // built-in FastLED rainbow, plus some random sparkly glitter
  rainbow();
  addGlitter(80);
}

void addGlitter( fract8 chanceOfGlitter) 
{
  if( random8() < chanceOfGlitter) {
    leds[ random16(NUM_LEDS) ] += CRGB::White;
  }
}

void confetti() 
{
  // random colored speckles that blink in and fade smoothly
  fadeToBlackBy( leds, NUM_LEDS, 10);
  int pos = random16(NUM_LEDS);
  leds[pos] += CHSV( gHue + random8(64), 200, 255);
}

void sinelon()
{
  // a colored dot sweeping back and forth, with fading trails
  fadeToBlackBy( leds, NUM_LEDS, 20);
  int pos = beatsin16( 13, 0, NUM_LEDS-1 );
  leds[pos] += CHSV( gHue, 255, 192);
}

void bpm()
{
  // colored stripes pulsing at a defined Beats-Per-Minute (BPM)
  uint8_t BeatsPerMinute = 62;
  CRGBPalette16 palette = PartyColors_p;
  uint8_t beat = beatsin8( BeatsPerMinute, 64, 255);
  for( int i = 0; i < NUM_LEDS; i++) { //9948
    leds[i] = ColorFromPalette(palette, gHue+(i*2), beat-gHue+(i*10));
  }
}

void juggle() {
  // eight colored dots, weaving in and out of sync with each other
  fadeToBlackBy( leds, NUM_LEDS, 20);
  byte dothue = 0;
  for( int i = 0; i < 8; i++) {
    leds[beatsin16( i+7, 0, NUM_LEDS-1 )] |= CHSV(dothue, 200, 255);
    dothue += 32;
  }
}

~~~~

the connection:
![the connection](./esp-rgb-conn.jpg)

power the strip with a 5v-2a wallwart

## Chicken tubes
install fastled, install esp8266 support in your arduino IDE, be lazy :tada:
nothing fancy, just a modified fastled demoreel
same code as the rgb strip, just change this #define to
~~~~
#define LED_TYPE    TM1809
~~~~

chicken tubes are on display in the space they have their own 12v powerpack.

## Lightbrellas

This is basically an esp8266 acting as a UDP -> UART bridge towards a bluepill
used a bluepill cause i needed 15 pwm channels, we had some in the hardware selfvending cabinet. i like STM32, byte me :p

ESP code :
~~~~
#include <ESP8266WiFi.h>
#include <WiFiUdp.h>

const int ledPin =  LED_BUILTIN;
char ssid[] = "HSBXL-IoT"; //SSID of your Wi-Fi router
char pass[] = "unguessable_password"; //Password of your Wi-Fi router
byte packetBuffer[16];

const long interval = 1000;
volatile int ledState = LOW;
WiFiUDP Udp;
volatile unsigned long previousMillis = 0;        // will store last time LED was updated
volatile unsigned long currentMillis;
char incomingPacket[255];

void connectAP(){
  Serial.println();
  Serial.println();
  Serial.print("Connecting to...");
  Serial.println(ssid);
  WiFi.begin(ssid, pass);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("Wi-Fi connected successfully");
  Serial.println("IP address: ");
    Serial.println(WiFi.localIP()); 
    Serial.println(WiFi.subnetMask());
  }



  void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200); 
  pinMode(LED_BUILTIN, OUTPUT); 
  connectAP();
  digitalWrite(ledPin, ledState);
  Udp.begin(1234);
}

void loop() {
  // put your main code here, to run repeatedly:
  //
  currentMillis = millis();
  int packetSize = Udp.parsePacket();
  if (packetSize)
  {

    int len = Udp.read(incomingPacket, 255);
    if (len > 0)
    {
     incomingPacket[len] = 0;
    if(incomingPacket[0] =='b'){
     Serial.write("b");
     if (ledState == LOW) {
        ledState = HIGH;
      } else {
        ledState = LOW;
      }
     digitalWrite(ledPin, ledState);
     }
    }
  }
  if (currentMillis - previousMillis >= interval) {
    // save the last time you blinked the LED
    previousMillis = currentMillis;
    int s = WiFi.status();
    if( (s == WL_DISCONNECTED) || (s == WL_CONNECTION_LOST) ){
     // WiFi.stop();  
      connectAP();
    }
    
  }
    delay(5);
}
~~~~

bluepillcode

[here](https://github.com/Jegeva/bytelights)

program with a recycled stm32 devboard or a blackmagic or a cheap stlink clone...

connections :

![the system](./lightbrellas.jpg)

the 5 umbrellas plug in the cross, yeah have 1 power (green cable) 1 ground (brown cable) that cannot be mixed thanks to male femal connector.

The long multicolored ribbon goes to the logic board.

![the logic board](./loic-lightbrellas.jpg)

umbreallas calbes are numbers 1 to 5, 1 is in the center and after that just go in order.

on the logic board 1 goes to the side without the black cable (ground , not used afte rdev, was just for my scope) then 2,3,4 and 5 last pin is NEXT to the black cable.

![the power brick](./power-lightbrellas.jpg)

plug in the wall :p

## Bass detection circuit

Thanks and Kudos to LenarT for the help with the opamp design, i suck at analog
It have an esp8266 with an interrup attached on a pin, waits for it, broadcasts an UDP bass packet, other stuff blinks


Arduino code
~~~~
#include <ESP8266WiFi.h>

#include <WiFiUdp.h>
IPAddress local_IP(192,168,4,1);
IPAddress gateway(192,168,4,254);
IPAddress subnet(255,255,255,0);

const int ledPin =  LED_BUILTIN;// the number of the LED pin
const int interpin = 15;

// Variables will change:
volatile int ledState = LOW;             // ledState used to set the LED

// Generally, you should use "unsigned long" for variables that hold time
// The value will quickly become too large for an int to store
volatile unsigned long previousMillis = 0;        // will store last time LED was updated
volatile unsigned long currentMillis;
// constants won't change:
const long interval = 1000; 

char ssid[] = ""; //SSID of your Wi-Fi router
char pass[] = ""; //Password of your Wi-Fi router

IPAddress broadcast(192,168,40,255);
volatile uint8_t m = 1;
  WiFiUDP Udp;
  char  replyPacket[] = "b";
  void interpinSR(){
 //   if(m==1){
   //   m=0;
   if (currentMillis - previousMillis >= 100) { 
      if (ledState == LOW) {
        ledState = HIGH;
      } else {
        ledState = LOW;
      }
     digitalWrite(ledPin, ledState);
     currentMillis = millis();
     previousMillis = currentMillis;
      Udp.beginPacket(broadcast, 1234);  Udp.write(replyPacket); Udp.endPacket();
      //Serial.print("b");
  //   m=1;
   }
    }

void connectAP(){
  Serial.println();
  Serial.println();
  Serial.print("Connecting to...");
  Serial.println(ssid);
  WiFi.begin(ssid, pass);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("Wi-Fi connected successfully");
  Serial.println("IP address: ");
    Serial.println(WiFi.localIP()); 
    Serial.println(WiFi.subnetMask());
  }
  

byte packetBuffer[1];
void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200); 
  pinMode(LED_BUILTIN, OUTPUT); pinMode(interpin, INPUT);
  connectAP();
  attachInterrupt(digitalPinToInterrupt(interpin),interpinSR,RISING);
   packetBuffer[0]='b';
}


void loop() {
  // put your main code here, to run repeatedly:
  //
  delay(100);
  //Serial.print('t');
  currentMillis = millis();
  memset(packetBuffer, 0, 1);
  if (currentMillis - previousMillis >= interval) {
    
    // save the last time you blinked the LED
    previousMillis = currentMillis;
// Udp.beginPacket(broadcast, 1234); //NTP requests are to port 123
  //Serial.println("4");
 // EthernetUdp.write(packetBuffer, 1);
  //Serial.println("5");
 // EthernetUdp.endPacket();
        
    // if the LED is off turn it on and vice-versa:
    // set the LED with the ledState of the variable:
    int s = WiFi.status();
    if( (s == WL_DISCONNECTED) || (s == WL_CONNECTION_LOST) ){
     // WiFi.stop();
     
      connectAP();
     }
  }
}
~~~~
the circuit:
![the circuit](./bassdetect.jpg)
the pot is changing the detection threshold

the schematic (top, down is bandpass for hihat for next year):
![the schem](./schem-bassdetect.jpg)

power : any 5v wall wart will do.


# TODO 

# Current version 
