---
title: "Company membership"
linktitle: "Company membership"
---

With a company membership, you get all the benefits of a [regular membership](/membership),  
plus we send you a monthly payment proof, valid for Belgian bookkeeping.

    Some important remarks regarding a company membership:
    - A company membership has no privileges compared to a regular membership.  
    - A company membership is always for one person only. 
      If you want to give access to multiple employees, pay for multiple memberships.  

# Price
For company membership, HSBXL asks a minimum of €40/month per person.  
A company can pay for multiple persons, in which case the memberships will be combined to one invoice.

# Start
Send a mail to finance@hsbxl.be with following info, needed for the invoice:

- Company name
- Company address
- BTW/TVA (tax) number
- Name of the person you want make a member


